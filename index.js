// console.log("Hello World!");
// Session 23 - Activity - JS Objects

// Real World Application Of Objects
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log(this.pokemon[0] + '! I choose you!')
	}
	
};
console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notation: ");
console.log(trainer['pokemon']);

console.log("Result of talk method: ");
trainer.talk();

/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + "'s health is now reduced to " + (target.health = target.health - this.attack));
        if (target.health <= 0){
            console.log(target.name + ' fainted.');
        }
        };
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }

}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 12);
let charizard = new Pokemon("Charizard", 50);
let squirtle = new Pokemon("Squirtle", 10);
let bulbasaur = new Pokemon("Bulbasaur", 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);
let rattata = new Pokemon('Rattata', 8);
console.log(pikachu);
console.log(charizard);
console.log(squirtle);
console.log(bulbasaur);
console.log(geodude);
console.log(mewtwo);
console.log(rattata);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
// pikachu.tackle(rattata); -> Pikachu tackled Rattata

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);